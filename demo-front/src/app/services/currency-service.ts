import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RecordType } from '../utils/record-types';

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {
  private postUrl =
    'http://localhost:8080/currencies/get-current-currency-value-command';
  private getUrl = 'http://localhost:8080/currencies/requests';

  private newRecordSubject = new Subject<void>();

  constructor(private http: HttpClient) {}

  publishNewRecord() {
    this.newRecordSubject.next();
  }

  subscribeToNewRecords() {
    return this.newRecordSubject.asObservable();
  }

  sendRecord(body: {
    currency: string;
    name: string;
  }): Observable<{ value: number }> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.http.post<{ value: number }>(this.postUrl, body, { headers });
  }

  getRecords(): Observable<Array<RecordType>> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.http.get<Array<RecordType>>(this.getUrl, { headers });
  }
}
