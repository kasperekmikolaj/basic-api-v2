import { Component } from '@angular/core';
import { CurrencyService } from '../services/currency-service';
import { DateExtractor } from '../utils/date-extractor';
import { RecordType, RecordTypeWithDate } from '../utils/record-types';

@Component({
  selector: 'app-record-list',
  templateUrl: './record-list.component.html',
  styleUrls: ['./record-list.component.scss'],
})
export class RecordListComponent {
  records: Array<RecordTypeWithDate> = [];

  constructor(private currencyService: CurrencyService) {}

  ngOnInit(): void {
    this.currencyService
      .subscribeToNewRecords()
      .subscribe(() => this.updateRecords());
    this.updateRecords();
  }

  private updateRecords() {
    this.currencyService.getRecords().subscribe((data: Array<RecordType>) => {
      this.records = [];
      for (let d of data) {
        this.records.push({
          currency: d.currency,
          name: d.name,
          date: DateExtractor.extractDate(d.date),
          value: d.value,
        });
      }
    });
  }
}
