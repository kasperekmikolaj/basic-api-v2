export type RecordType = {
  currency: string;
  name: string;
  date: string;
  value: number;
};

export type RecordTypeWithDate = {
  currency: string;
  name: string;
  date: Date;
  value: number;
};
