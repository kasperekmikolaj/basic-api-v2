export class DateExtractor {
  static extractDate(dateComponents: string): Date {
    const year = parseInt(dateComponents[0]);
    const month = parseInt(dateComponents[1]);
    const day = parseInt(dateComponents[2]);
    const hour = parseInt(dateComponents[3]);
    const minute = parseInt(dateComponents[4]);
    const second = parseInt(dateComponents[5]);
    const millisecond = parseInt(dateComponents[6]);
    const date = new Date(year, month, day, hour, minute, second, millisecond);
    return date;
  }
}
