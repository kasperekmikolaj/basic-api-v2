import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RecordFormComponent } from './record-form/record-form.component';
import { RecordListComponent } from './record-list/record-list.component';

@NgModule({
  declarations: [AppComponent, RecordListComponent, RecordFormComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
