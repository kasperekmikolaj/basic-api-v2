import { Component } from '@angular/core';
import { CurrencyService } from '../services/currency-service';

@Component({
  selector: 'app-record-form',
  templateUrl: './record-form.component.html',
  styleUrls: ['./record-form.component.scss'],
})
export class RecordFormComponent {
  constructor(private currencyService: CurrencyService) {}

  formData = {
    currency: '',
    name: '',
  };

  handleFormSubmittion() {
    this.currencyService.sendRecord(this.formData).subscribe(() => {
      this.currencyService.publishNewRecord();
    });
    this.formData.currency = '';
    this.formData.name = '';
  }
}
