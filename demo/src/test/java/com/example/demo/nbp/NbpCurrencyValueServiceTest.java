package com.example.demo.nbp;

import com.example.demo.exceptions.CurrencyCodeNotFoundException;
import com.example.demo.exceptions.ExternalApiException;
import com.example.demo.services.CurrencyValueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
@SpringBootTest
class NbpCurrencyValueServiceTest {

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private CurrencyValueService currencyValueService;

    @Test
    void shouldThrowCurrencyCodeNotFoundExceptionWhenNoCurrencyFound() {
        // given
        NbpTableModel tableModel = new NbpTableModel();
        NbpCurrencyValueModel currencyValueModel = new NbpCurrencyValueModel();
        currencyValueModel.setCode("something");
        tableModel.setRates(List.of(currencyValueModel));
        ResponseEntity<NbpTableModel[]> response
                = new ResponseEntity<>(new NbpTableModel[]{tableModel}, HttpStatus.OK);
        when(restTemplate.getForEntity(anyString(), eq(NbpTableModel[].class))).thenReturn(response);
        // when then
        assertThatThrownBy(() -> currencyValueService.getCurrencyValue("not_existing"))
                .isInstanceOf(CurrencyCodeNotFoundException.class);
    }

    @Test
    void shouldThrowExternalApiExceptionWhenBodyLengthIsZero() {
        // given
        ResponseEntity<NbpTableModel[]> response
                = new ResponseEntity<>(new NbpTableModel[]{}, HttpStatus.OK);
        when(restTemplate.getForEntity(anyString(), eq(NbpTableModel[].class))).thenReturn(response);
        // when then
        assertThatThrownBy(() -> currencyValueService.getCurrencyValue("not_existing"))
                .isInstanceOf(ExternalApiException.class);
    }

    @Test
    void shouldReturnCorrectValueForCurrencyCode() {
        // given
        NbpTableModel tableModel = new NbpTableModel();
        NbpCurrencyValueModel currencyValueModel = new NbpCurrencyValueModel();
        currencyValueModel.setCode("eur");
        currencyValueModel.setMid(22.2);
        NbpCurrencyValueModel currencyValueModel2 = new NbpCurrencyValueModel();
        currencyValueModel2.setCode("usd");
        tableModel.setRates(List.of(currencyValueModel, currencyValueModel2));
        ResponseEntity<NbpTableModel[]> response
                = new ResponseEntity<>(new NbpTableModel[]{tableModel}, HttpStatus.OK);
        when(restTemplate.getForEntity(anyString(), eq(NbpTableModel[].class))).thenReturn(response);
        // when
        double result = currencyValueService.getCurrencyValue("eur");
        // then
        assertThat(result).isEqualTo(22.2);
    }
}


























