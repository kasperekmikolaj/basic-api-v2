package com.example.demo.service;

import com.example.demo.Database;
import com.example.demo.RecordModel;
import com.example.demo.dto.RequestDto;
import com.example.demo.dto.ValueDto;
import com.example.demo.services.CurrencyValueService;
import com.example.demo.services.RecordService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class RecordServiceTest {

    @MockBean
    private CurrencyValueService currencyValueService;

    @MockBean
    private Database database;

    @Autowired
    private RecordService recordService;

    @Test
    void shouldReturnCurrencyValue() {
        // given
        RequestDto requestDto = new RequestDto("cur", "name");
        requestDto.setName("banana");
        requestDto.setCurrency("yegood");
        when(currencyValueService.getCurrencyValue(anyString())).thenReturn(30.3);
        // when
        ValueDto result = recordService.handleRequest(requestDto);
        // then
        assertThat(result.getValue()).isEqualTo(30.3);
        verify(database, times(1)).saveRecord(any());
    }

    @Test
    void shouldGetAllRecords() {
        // given
        RecordModel recordModel = new RecordModel("eur", "eurasy", LocalDateTime.now(), 22.2);
        RecordModel recordModel2 = new RecordModel("eur", "eurasy", LocalDateTime.now(), 22.2);
        when(database.getRecords()).thenReturn(List.of(recordModel, recordModel2));
        // when
        List<RecordModel> result = recordService.getRecords();
        // then
        assertThat(2).isEqualTo(result.size());
        assertThat(result).contains(recordModel);
        assertThat(result).contains(recordModel2);
    }
}
