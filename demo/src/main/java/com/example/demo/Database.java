package com.example.demo;

import java.util.List;

public interface Database {

    public void saveRecord(RecordModel recordModel);

    public List<RecordModel> getRecords();

}
