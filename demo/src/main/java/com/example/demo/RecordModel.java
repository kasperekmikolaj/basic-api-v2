package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class RecordModel {

    private String currency;
    private String name;
    private LocalDateTime date;
    // do not store money in double xD
    private Double value;

}
