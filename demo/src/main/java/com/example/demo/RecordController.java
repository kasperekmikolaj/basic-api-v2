package com.example.demo;

import com.example.demo.dto.RequestDto;
import com.example.demo.dto.ValueDto;
import com.example.demo.services.RecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("currencies")
@AllArgsConstructor
public class RecordController {

    private final RecordService recordService;

    @PostMapping("get-current-currency-value-command")
    public ValueDto getCurrentValue(@RequestBody RequestDto requestDto) {
        return recordService.handleRequest(requestDto);
    }

    @GetMapping("requests")
    public List<RecordModel> getRequests() {
        return recordService.getRecords();
    }
}
