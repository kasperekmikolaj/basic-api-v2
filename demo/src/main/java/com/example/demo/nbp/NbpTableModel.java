package com.example.demo.nbp;

import lombok.Data;

import java.util.List;
@Data

public class NbpTableModel {
    private String table;
    private String no;
    private String effectiveDate;
    private List<NbpCurrencyValueModel> rates;
}
