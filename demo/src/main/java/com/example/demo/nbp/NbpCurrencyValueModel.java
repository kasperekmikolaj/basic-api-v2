package com.example.demo.nbp;

import lombok.Data;

@Data
public class NbpCurrencyValueModel {
    private String currency;
    private String code;
    private double mid;
}
