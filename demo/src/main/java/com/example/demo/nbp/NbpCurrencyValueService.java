package com.example.demo.nbp;

import com.example.demo.exceptions.CurrencyCodeNotFoundException;
import com.example.demo.exceptions.ExternalApiException;
import com.example.demo.services.CurrencyValueService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
@AllArgsConstructor
public class NbpCurrencyValueService implements CurrencyValueService {

    private static final String NBP_URL = "http://api.nbp.pl/api/exchangerates/tables/A?format=json";
    private final RestTemplate restTemplate;

    @Override
    public double getCurrencyValue(String currencyCode) throws RuntimeException {
        ResponseEntity<NbpTableModel[]> response = restTemplate.getForEntity(
                NBP_URL, NbpTableModel[].class);
        if(Objects.isNull(response.getBody()) || response.getBody().length == 0) {
            throw new ExternalApiException();
        }

        return response.getBody()[0].getRates()
                .stream()
                .filter(currencyValueModel -> currencyValueModel.getCode().equals(currencyCode))
                .map(NbpCurrencyValueModel::getMid)
                .findFirst()
                .orElseThrow(CurrencyCodeNotFoundException::new);
    }
}
