package com.example.demo.impl;

import com.example.demo.exceptions.BadRequestException;
import com.example.demo.services.CurrencyValueService;
import com.example.demo.Database;
import com.example.demo.RecordModel;
import com.example.demo.services.RecordService;
import com.example.demo.dto.RequestDto;
import com.example.demo.dto.ValueDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class RecordServiceImpl implements RecordService {

    private final Database database;
    private final CurrencyValueService currencyValueService;

    @Override
    public ValueDto handleRequest(RequestDto requestDto) throws RuntimeException {
        if (Objects.isNull(requestDto.getCurrency()) || Objects.isNull(requestDto.getName())
                || requestDto.getCurrency().isEmpty() || requestDto.getName().isEmpty()) {
            throw new BadRequestException();
        }
        double currencyValue = currencyValueService.getCurrencyValue(requestDto.getCurrency());
        RecordModel recordModel = new RecordModel(requestDto.getCurrency(), requestDto.getName(), LocalDateTime.now(), currencyValue);
        database.saveRecord(recordModel);
        return new ValueDto(currencyValue);
    }

    @Override
    public List<RecordModel> getRecords() {
        return database.getRecords();
    }
}
