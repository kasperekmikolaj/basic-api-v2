package com.example.demo.impl;

import com.example.demo.Database;
import com.example.demo.RecordModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component
public class PseudoDatabaseImpl implements Database {

    private final ConcurrentLinkedQueue<RecordModel> database = new ConcurrentLinkedQueue<>();

    @Override
    public void saveRecord(RecordModel recordModel) {
        database.add(recordModel);
    }

    @Override
    public List<RecordModel> getRecords() {
        return new ArrayList<>(database);
    }
}
