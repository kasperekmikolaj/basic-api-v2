package com.example.demo.services;

public interface CurrencyValueService {
    public double getCurrencyValue(String currencyCode) throws RuntimeException;
}
