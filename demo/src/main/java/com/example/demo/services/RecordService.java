package com.example.demo.services;

import com.example.demo.RecordModel;
import com.example.demo.dto.RequestDto;
import com.example.demo.dto.ValueDto;

import java.util.List;

public interface RecordService {

    public ValueDto handleRequest(RequestDto requestDto) throws RuntimeException;

    public List<RecordModel> getRecords();

}
